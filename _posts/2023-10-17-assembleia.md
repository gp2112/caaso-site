---
layout: post
title: Assembleia do dia 17/10/2023
author: CAASO
thumbnail: assets/imgs/assembleia-2023-10-11.png
feature_image: "assets/imgs/20230507_00_movimento_estudantil_usp.webp"
tags: ["Assembleia"]
---

**RESUMO DA ASSEMBLEIA (17/10)**

Após reunião com a prefeitura do campus e unidades, foi realizada uma assembleia no palquinho para deliberar os próximos passos da luta estudantil no CAASO.


Aqui trazemos uma contextualização da paralisação e da reunião com a prefeitura, assim como os encaminhamentos aprovados na Assembleia Geral do CAASO de ontem. Ao final do post, está a ata completa da assembleia para quem desejar ficar mais a par dos detalhes (incluindo as reivindicações atendidas, contempladas nas duas primeiras falas de abertura).


É importante esclarecer que não rebaixaremos nossa linha: Se nossas pautas locais não forem atendidas, pautaremos nova paralisação futuramente. Existem muitas mudanças a ocorrer na reitoria, porém, por hoje, muitas das nossas reivindicações foram alcançadas devido à nossa luta. Isso é resultado da organização dos estudantes.


Confira abaixo os encaminhamentos aprovados em assembleia:


[Ata completa com as falas em PDF](https://drive.google.com/file/d/1556HGJySxNI55kTiTSPaDd59nIAB_INP/view)


## Encaminhamentos Aprovados

**Encaminhamento 0:** Criação de um documento, a ser levado para a prefeitura, que firme os compromissos da mesma com o cumprimento dos pontos as quais a compete, definidos na reunião de 17/10.

**Encaminhamento 1:** Criação de um fórum de Representantes Discentes (RDs).

**Encaminhamento 2:** Realização mínima de Conselho de Secretarias Acadêmicas (CSAs) a cada 45 dias.

**Encaminhamento 3:** Estabelecimento de Boletim CAASO que periodicamente trará repasses relacionados às reuniões com a prefeitura, de RDs, CSAs, e demais assuntos pertinentes. 

**Encaminhamento 4:** Que seja realizada uma assembleia daqui a três semanas para avaliar o andamento das pautas. 

**Encaminhamento 5:** Que seja realizada uma assembleia mensal de repasses, que também deve coletar novas demandas. Essa assembleia deve ser acompanhada de, depois, um evento cultural.

**Encaminhamento 6:** Caso não haja a aprovação de um dos quatro orçamentos planejados pela reitoria ao Aloja nem uma previsão concreta de uma data para a reforma dos telhados de seus blocos, será pautada uma greve em assembleia geral.
