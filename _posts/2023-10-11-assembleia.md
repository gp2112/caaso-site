---
layout: post
title: Assembleia do dia 10/10/2023
author: CAASO
thumbnail: assets/imgs/assembleia-2023-10-11.png
feature_image: "assets/imgs/20230507_00_movimento_estudantil_usp.webp"
tags: ["Boletim"]
---

**Início:** 19:25 **Fim:** 23:30

**Mesa:** Açaí e Paupa

**Ata:** Angra, Estavare, João Nora e Lamp

[Ata completa com as falas em PDF](https://drive.google.com/file/d/1DCx1R5kVS4bxIunbdE08CZ2Z5SpKQdBQ/view)


## Pauta Única

1. Adesão à greve

## Encaminhamentos Aprovados

**Encaminhamento 0:** Paralisação quarta-feira 11/10 através de piquete nos prédios
de aula, com escrita de uma carta de reivindicações a ser levada para a prefeitura.

- Contratação de professores efetivos.
- Melhora na infraestrutura do Aloja.
- Revisão do contrato da empresa do bandejão: café da manhã universal e almoço e jantar aos sábados e domingos e feriados; contratação de mais funcionários do bandejão
da USP, assim como a revisão dos contratos do restaurante do restaurante universitário
para adequação da demanda. Inclusão de opção sem lactose, opção vegana e marmitas
do bandejão. Ampliação dos horários, e dos espaços do bandejão. Criação de um auxílio
emergencial para comer no bandejão, implementação de marmitas para quem nao
consegue comer no RU.
- Melhorias na infraestrutura do CEFER.
- Revisão do contrato do ônibus: mais motoristas e mais horários de ônibus, que os
horários sejam definidos com a participação dos estudantes, principalmente aqueles que
estudam no C2.

**Atividades para a Paralisação**

1. Ocupação cultural no palquinho durante a paralisação. (aprovado)
2. realizar uma plenária para conscientizar e agitar os estudantes em torno das
pautas da paralisação
3. piquetar os ônibus e permitir apenas a passagem de pesquisadores a
pós-graduação(aprovado)
4. Proposta de atividade cultural: Batuque e panelaço no C2 na frente da sala
do prefeito.(indicativo)(aprovado)
5. Ato unificado com DCE UFSCAR em defesa da permanência
estudantil.(indicativo)(aprovado)
6. Realizar feiras com exposição de arte dos alunos
artistas.(indicativo)(aprovado)
7. Dia de atividade para as crianças filhas dos estudantes. (indicativo)(aprovado)
8. Pular a catraca do bandejão amanhã no almoço e janta. (aprovado)


**Reinvidicações:**

- Contratação de professores efetivos.
- Melhora na infraestrutura do Aloja.
- Revisão do contrato da empresa do bandejão: café da manhã universal e almoço e jantar aos sábados e domingos e feriados; contratação de mais funcionários do bandejão da USP, assim como a revisão dos contratos do restaurante do restaurante universitário para adequação da demanda. Inclusão de opção sem lactose, opção vegana e marmitas do bandejão. Ampliação dos horários, e dos espaços do bandejão. Criação de um auxílio emergencial para comer no bandejão, implementação de marmitas para quem nao consegue comer no RU
- Melhorias na infraestrutura do CEFER.
- Revisão do contrato do ônibus: mais motoristas e mais horários de ônibus, que os horários sejam definidos com a participação dos estudantes, principalmente aqueles que estudam no C2.


**Encaminhamento 01:** Exigência de clareza no regimento sobre a definição dos
Créditos-Trabalho e a readequação dos Departamentos necessários a cada disciplina.
Justificativa: a carga horária dos professores é o principal mecanismo para
contratação de professores e alocação destes. O entendimento atual do regimento
não é claro e abre espaço para incongruências em relação a carga horária extraclasse dos discentes e contabilização de horas de docentes.

**Encaminhamento 02:** Contratação em caráter de urgência de Professores para o
departamento de Geotecnia (SGS).


**Encaminhamento 03:** Revisão do Plano de Ocupação do Campus II e cobrança do
mesmo, garantindo a infraestrutura adequada para os alunos, funcionários e
docentes (mais horários de ônibus e maior qualidade da frota, bandejão na janta,
oferecimento de serviços de saúde, línguas e suporte).


**Encaminhamento 04:** Melhorias no CEFER:
- Plena abertura do campão e manutenção adequada de banheiros, vestiário no
campo, conserto dos refletores, manutenção da grama do campo.
- Contratação de profissionais para possibilitar a abertura do CEFER em
feriados e fins de semana.

**Encaminhamento 05:** Adição das pautas do Aloja como parte central das
reivindicações de São Carlos, que incluem os eixos de infraestrutura, contratação de
funcionários e atendimento psicológico.

**Encaminhamento 07:** Reivindicar o aumento da representação discente nos
colegiados por meio de aumento da porcentagem de validade dos votos (um voto
discente valer mais de um voto de maneira geral).

**Encaminhamento 08:** Reivindicar atendimento na UBAS. Atendimento pra
questões básicas.

**Encaminhamento 10:** Reivindicar atendimento psicológico em todos os institutos
de forma a atender a demanda dos alunos.

**Encaminhamento 11:** Incluir as demandas da pós-graduação (APG).

**Encaminhamento 12:** Reivindicar a contratação de professoras para creche.

**Encaminhamento 13:** Que seja realizada a eleição de representantes de sala, os
quais vão estar em contato com o CAASO e as SAs.

**Encaminhamento 14:** Exigir um compromisso das diretorias dos institutos com
reuniões mensais com RDs, SAs e CAASO.

**Encaminhamento 15:** Encaminhamento para os RDs responsáveis as demandas
aqui apresentadas para a ciência das autoridades até o final da semana, e
posteriormente o retorno ao CAASO das respostas das autoridades.

**Encaminhamento 16:** Ato unificado com DCE UFSCAR em defesa da
permanência estudantil.

**Encaminhamento 17:** Realizar feiras com exposição de arte dos alunos artistas

**Encaminhamento 18:** Jornal da greve, caso ocorra.

**Encaminhamento 19:** Dia de atividade para as crianças filhas dos estudantes.

**Encaminhamento 20:** Realizar trancaço no caso de paralisação.
- Adição: Que o trancaço seja realizado antes de qualquer aula começar.

**Encaminhamento 21:** Definição de uma porcentagem obrigatória a contratação de
professores com caráter pedagógico.

**Encaminhamento 25:** Incluir o conserto do portão da geotec nas reivindicações.




