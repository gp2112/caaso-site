---
layout: post
title: Sobre o Boletim CAASO
author: CAASO
thumbnail: assets/imgs/booletim.png
feature_image: "assets/imgs/20230507_00_movimento_estudantil_usp.webp"
tags: ["Boletim"]
---

## O que é o Boletim CAASO e a nossa gestão?

Como encaminhado na última Assembleia Geral (17/10/2023), teremos a realização do Boletim CAASO, 
a publicação periódica (pretendemos mantê-la como quinzenal!) de um breve jornal, 
como este que você está lendo agora: a primeiríssima edição!

O objetivo é tanto político quanto cultural, sendo um propagandista,
agitador e organizador coletivo do corpo estudantil dos campi da USP em São Carlos (representado pelo
CAASO - Centro Acadêmico
Armando de Salles Oliveira) e da
comunidade em que estamos
inseridos, incluindo, de forma
mais geral, toda a população com
a qual o meio universitário se envolve.

Por aqui, você vai encontrar repasses políticos, para
sempre estar por dentro do que tem rolado aqui em assembleias
gerais, reuniões da diretoria, fórum de RDs (Representantes
Discentes) e CSAs (Conselho Secretarias Acadêmicas), além de
denúncias, notas de apoio e repúdio e outros acontecimentos
pontuais da comunidade da USP, em especial do CAASO. E para
além da mera política, a cultura é essencial para que nos
integremos num corpo coeso. Assim, teremos aqui a divulgação
de muitos eventos culturais realizados por nós do CA (Centro
Acadêmico), pelas SAs (Secretarias Acadêmicas), pelos coletivos e
por grupos independentes, para que você não se limite na
graduação a um estudo mecanizado e cansativo. Não podemos parar de lutar para que possamos aproveitar, de fato, cada vez
mais tudo que este ambiente pode nos proporcionar.
