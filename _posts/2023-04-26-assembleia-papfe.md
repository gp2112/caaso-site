---
layout: post
title: Encaminhamentos da Assembleia Contra o Novo PAPFE
author: CAASO
thumbnail: "assets/imgs/assembleia-foto-1.jpg"
tags: [PAPFE, "Permanência Estudantil", "assembléia"]
---

![Assembleia do CAASO](/assets/imgs/assembleia-foto-1.jpg)

Na última quarta-feira, dia 26/04, foi realizada uma assembleia do CAASO para deliberarmos sobre mobilizações a serem realizadas devido aos problemas trazidos pelo PAPFE III!

Esse tema é de caráter urgente e emergencial! Com as novas alterações diversos estudantes não contemplados com o programa estão em situação de vulnerabilidade, não tendo mais certeza se serão capazes de completar sua graduação por falta de recursos para alimentação e moradia!!

Na prática, a Universidade está deliberadamente expulsando pessoas de baixa renda, construindo um projeto segregador e elitista!!

Assim, se torna essencial que todes es estudantes se compadeçam da situação em que se encontram seus colegas e participem ativamente dessas mobilizações e cobrem suas entidades de realizarem tarefas em pról das reinvindicações tiradas em assembleias!!

---

### Encaminhamentos

1. Deve-se exigir **total transparência** sobre o processo do PAPFE, com critérios de **seleção** e de **pontuação**.
2. Deve-se exigir que **todas as pessoas** que pediram **recurso** sejam **contempladas**.
3. Devemos dar continuidade à pauta da **contraproposta** elaborada pelo **Alojamento** sobre o novo PAPFE, fazendo com quue nossas **antigas exigências** também sejam atendidas.
4. Devemos pautar a **mudança da COIP (Conselho de Inclusão e Pertencimento)** de maneira que a gente tenha uma **representação mista e paritária** no que diz respeito sobre as **deliberações realizadas na PRIP**.
5. Que seja(m) deliberado(s) o(s) dia(s) de paralisação de acordo com o que forr definido nas assembleias das SAs. Que também contatem as CGs e peçam a **paralisação das atividades** letivas no(s) dia(s) definidos para paralisação. A partir delas, avaliar a adesão para **greve geral e piquetes**. Que nas paralisações também haja **atividades cultuurais**, como: batalhas de rap, samba, karaokê, etc. a serem definidas pelo **GT - CAASO contra o Novo PAPFE**.
6. As SAs **devem convocar assembleia de cursos** sobre o tema do PAPFE.
7. Que o **GT - CAASO Contra o Novo PAPFE** tenha **reuniões periódicas** em **horários fixos** e que cada SA tenha um representante neste GT, **responsável** para repassar à SA que ela representa. Além disso, que seja feito **passagens em salas** falando do grupo e pedindo para que os estudantes **cobrem das SAs** atualizações acerca de mobilizações pelo PAPFE.

---

### Como Ajudar e Mais Informações

- [GT - CAASO Contra o Novo PAPFE - Grupo no WhatsApp](https://chat.whatsapp.com/GkcmYZ9BiJtIPQ56tFEyVf)
- [Drive do GT - CAASO Contra o Novo PAPFE](https://drive.google.com/drive/u/1/folders/1J1SHts5z11E_7BkI3zfXdZ20ZQYMwCTz)
- [Forms de Levantamento de qualificados e lista de espera no CAASO](https://docs.google.com/forms/d/e/1FAIpQLSee4ANURI-CUFivHjiIBRA0FN6uowU3O3UpmlZBen3bdhqpww/viewform)
- [Ajude a construir o CAASO!](https://docs.google.com/forms/d/e/1FAIpQLSdHqikdumAIMuRmRn_ZlmmbnX6ZfwBUdVl1ieX3CuFHMFt_Ew/viewform)
