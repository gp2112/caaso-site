---
layout: post
title: A Recente Greve na USP - um resumo
author: CAASO
thumbnail: assets/imgs/greve-usp-2023.jpg
feature_image: "assets/imgs/20230507_00_movimento_estudantil_usp.webp"
tags: ["Greve USP"]
---

Em agosto, no campus Butantã, ocorreu uma assembleia
que encaminhou um indicativo de greve para o dia 21 de
setembro. Deste momento, vários campi começaram a fazer
assembleias para agitar para que essa greve ocorresse, porém
tendo resultados diferentes e tendo institutos e campi com
estudantes mais mobilizados que outros.
Porém, a 3 dias do indicativo, no dia 18 de setembro, com a
finalidade de impedir que as assembleias de cursos e a greve
ocorresse, o diretor da FFLCH (Faculdade de Filosofia, Letras e Ciências Humanas) ordenou que a guarda universitária fechasse
os prédios e fizesse com que durante 2 dias não tivesse aulas.
Assim, os alunos fizeram uma assembleia, que teve uma enorme
participação, para tirar uma greve imediata. A nota da FFLCH
alegou, diante de uma mobilização pacífica dos estudantes, risco
de dano ao patrimônio público.

![e-mail da USP dizendo que cancelou as aulas durante a semana que seria a greve](/assets/imgs/greve-usp-email.png)

Os estudantes da FFLCH imediatamente paralisaram os
prédios e, a partir desse evento, os outros cursos e institutos
começaram a fazer assembleias para aderir à greve. O fervor da
greve foi tão grande que até institutos como a Escola Politécnica
aderiram a greve; e outros campi, como Ribeirão Preto e o
CAASO fizeram movimentações próprias e trazendo suas
próprias reivindicações locais.

Durante a greve, foram feitas reuniões de negociação
junto à reitoria que inicialmente não avançaram muito. Essa
demora ocorreu principalmente porque o reitor não estava no
Brasil e a vice-reitoria não colaborava com os estudantes,
trazendo uma tática para cansá-los. Em outros campi, a
movimentação teve resultados diferentes. Em Ribeirão Preto, os
estudantes ocuparam a prefeitura do campus e conseguiram
algumas de suas reivindicações e, aqui no CAASO, nosso único dia de paralisação fez com que a prefeitura do campus de São
Carlos cedesse algumas de nossas reivindicações.

Durante as 6 semanas que a greve ocorreu, alguns
institutos conseguiram suas reivindicações mais cedo. Porém as
reivindicações junto à reitoria como a contratação de
professores foram realizadas após muita pressão dos estudantes.
No fim de outubro, após 6 semanas de greve, ela terminou com
um saldo muito positivo, tendo estudantes mobilizados e que
sabem que a força coletiva dos estudantes podem avançar pautas
importantes para uma universidade pública com cada vez mais
qualidade. Para a proposta completa da reitoria, leia em
[https://cefisma.org.br/boletim/greve/documento-apresentado-pela-reitoria-frente-a-comissao-de-negociacao/](https://cefisma.org.br/boletim/greve/documento-apresentado-pela-reitoria-frente-a-comissao-de-negociacao/).

## As últimas assembleias do CAASO

![Imagemm do palquinho cheio durante a assembleia](/assets/imgs/assembleia-caaso-2023.png)

Como produto de constante descontentamento dos
alunos e forma de apoio aos demais pólos universitários, em
10/10/2023, houve uma [assembleia geral do CAASO](/2023/10/11/assembleia.html), que contou com a presença de
aproximadamente 600 estudantes. Tal sessão os
deixou a par da conjuntura de
paralisações em outros campi
universitários e teve como
objetivo final debater acerca
dos problemas presentes no
campus de São Carlos, como:

- Insatisfação dos alunos com a alimentação do RU (Restaurante Universitário), com a falta de espaço para todos os alunos, e com o fechamento dele em feriados e
fins de semanas e falta do café da manhã para todos alunos;
- Falta de horários dos ônibus que levam os alunos do Campus 1 para o Campus 2, além da carência de mais
ônibus disponíveis para o transporte dos alunos;
- A precarização da infraestrutura do CEFER , assim como o fechamento da academia para os alunos;
- Valor dado no auxílio permanência (PAPFE), alunos que necessitam dos auxílios mas não são contemplados;
- Falta de professores especializados, além da precarização de diversos cursos oferecidos;
- Ausência de jantar no RU do Campus 2 e certo “abandono”
do C2;
- Carência de funcionários para o funcionamento adequado
da Creche, que não contrata desde 2011 e deixa em risco a
permanência de estudantes com filhos;
- Alojamentos com sérios problemas de infraestrutura ,como
alagamentos, vazamentos hidráulicos e sanitários;
- Falta de manutenção no portão da Geotecnia.


![Piquete realizado no IFSC](/assets/imgs/piquete-caaso-2023.png)

Depois de algumas falas dos representantes do CAASO
acerca dos problemas, assim
como falas abertas ao restante
dos alunos, houve a votação para
decidir a possibilidade de uma
paralisação com objetivo de
reivindicar melhorias para os
problemas apresentados nas
falas. Com o resultado da votação
a favor da paralisação, aprovada
por consenso, no dia 11 de
outubro houve mobilização nos
blocos de aulas e foram
reivindicadas, através de uma [carta](https://drive.google.com/file/d/18H7PiJHmUPfB6PkC5_uw8u1YreH4Det_/view)
enviada a Prefeitura do Campus, as seguintes exigências:

- Exigência de clareza no regimento sobre a definição dos
Créditos-Trabalho e a readequação dos Departamentos
necessários a cada disciplina.
- Revisão do Plano de Ocupação do Campus II, garantindo a
infraestrutura adequada para os alunos, funcionários e
docentes (mais horários de ônibus e maior qualidade da
frota, bandejão na janta, oferecimento de serviços de saúde,
línguas e suporte).
- Melhorias no CEFER:
 Plena abertura do campão e
manutenção adequada de banheiros, vestiário no campo,
conserto dos refletores, manutenção da grama do campo.
Contratação de profissionais para possibilitar a abertura do
CEFER em feriados e fins de semana.
- Contratação de professores efetivos com urgência.
- O alojamento foi parte das reivindicações de São Carlos, que
incluem os eixos de infraestrutura, contratação de
funcionários e atendimento psicológico.
- Aumento da representação discente nos colegiados por meio
de aumento da porcentagem de validade dos votos.
- Atendimento na UBAS para questões básicas.
- Atendimento psicológico em todos os institutos de forma a
atender a demanda dos alunos.
- Contratação de funcionários para a creche.
- Exigir um compromisso das diretorias dos institutos com
reuniões mensais com RDs, SAs e CAASO.
- Incluir o conserto do portão da geotecnia nas reivindicações.

No dia 17 de outubro, após a reunião da diretoria do
CAASO com a prefeitura, houve uma [assembleia](https://bit.ly/1710ata) para fazer um balanço da paralisação,
discutir sobre os resultados obtidos com a paralisação e como
correu a reunião com a prefeitura do campus. Na reunião, foram
discutidas diversas questões. Primeiramente, o problema
estrutural dos alojamentos, que era uma das principais
preocupações, que durante a paralisação, teve suas reivindicações legitimadas. O alojamento planejou se reunir com
a prefeitura para resolver imediatamente alguns problemas e
continuar discutindo outras questões.

Sobre a situação especificamente do alojamento e seu
diálogo com a prefeitura, redigiremos, juntos à sua Autogestão,
um texto para a segunda edição do boletim, prevista para o final
de novembro.
Em relação ao bandejão, foi ressaltado que é importante
distinguir as responsabilidades da prefeitura do campus e da
reitoria. O café da manhã universal foi aprovado pela prefeitura.
Além disso, a abertura do bandejão nos fins de semana e feriados
ficou de ser avaliada pela PRIP, com a prefeitura considerando
essa possibilidade. Também foi discutido o desconforto térmico
no espaço do bandejão, com a prefeitura comprometendo-se a
realizar um estudo de conforto térmico por meio da engenharia
da USP.

A questão do jantar no campus 2 foi abordada, resultando
na prefeitura concordando em realizar um período de teste com
jantar no C2 e horários de ônibus adequados para essa iniciativa.
Já o CEFER será aberto em feriados e uma lista de
prioridade de manutenção foi estabelecida para quadras, ginásio
e campo, devido a limitações orçamentárias. Quanto à
contratação na creche da UBAS, o edital será aberto no próximo
ano, sem perda de vagas, mas a contratação depende da reitoria.
A contratação de professores especializados e de licenciatura
está sendo analisada em conjunto com as unidades acadêmicas.
Não haverá retaliação aos participantes da paralisação, com
medidas para cobrir faltas e remarcação de provas asseguradas
aos estudantes do CAASO.

Além das falas sobre a reunião com a prefeitura, também
houve a discussão a respeito de como ocorreram as greves e diversos pontos foram levantados. Dentre estes pontos estão
vários elogios à paralisação e observações sobre algumas
exigências. Por outro lado, também houveram críticas acerca de
como foram feitas as votações, os piquetes e deram exemplos de
alguns inconvenientes que ocorreram durante as paralisações.
Por fim, foram votados alguns encaminhamentos para que todas
as demandas da assembleia fossem atendidas:

- Criação de um documento, que foi levado para a prefeitura,
para firmar os compromissos da mesma com o cumprimento
dos pontos aos quais compete, definidos na reunião de 17/10.
- Estabelecimento de Boletim CAASO que periodicamente
trará repasses relacionados às reuniões com a prefeitura, de
RDs, CSAs, e demais assuntos pertinentes.
- Que seja realizada uma assembleia mensal de repasses, que
também deve coletar novas demandas. Essa assembleia deve
ser acompanhada de, depois, um evento cultural.
- Caso não haja a aprovação de um dos quatro orçamentos
planejados pela reitoria ao Aloja nem uma previsão concreta
de uma data para a reforma dos telhados de seus blocos, será
pautada uma greve ou paralisação em uma nova assembleia
geral.

Esse [termo de compromisso](https://bit.ly/termoCAASO)
foi redigido pelo CAASO e entregue à prefeitura do campus em
30/10. Entretanto, conforme discutiremos na assembleia de
16/11, a prefeitura não se mostrou muito disposta e não marcou
até o presente momento uma reunião conosco para que eles
possam assinar o documento.
