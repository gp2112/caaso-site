{ stdenv, bundlerEnv, bundler, ruby, lib, baseurl ? null }: 


let
  gems = bundlerEnv {
      name = "caaso-site-env";
      inherit ruby;
      gemfile = ./Gemfile;
      lockfile = ./Gemfile.lock;
      gemset = ./gemset.nix;

  };
in
stdenv.mkDerivation {
  name = "caaso-site";
  buildInputs = [ gems bundler ruby ];
  src = ./.;
  JEKYLL_ENV = "production";
  buildPhase = ''
    ${gems}/bin/bundle exec jekyll build ${lib.optionalString (baseurl != null) "--baseurl ${baseurl}"}
  '';
  installPhase = ''
    mkdir -p $out
    cp -Tr _site $out/public
  '';
}
