{
  description = "Site institucional do CAASO";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    utils.url = "github:numtide/flake-utils";
  };


  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
  let pkgs = nixpkgs.legacyPackages.${system};
  in rec {
    packages = rec {
      caaso-site = pkgs.callPackage ./default.nix { };
      caaso-site-serve = pkgs.writeShellScriptBin "serve" ''
        echo "Serving on http://localhost:8000"
        ${pkgs.webfs}/bin/webfsd -F -f index.html -r ${caaso-site}/public
      '';
      remove-nbsp = pkgs.writeShellScriptBin "remove-nbsp" ''
        ${pkgs.gnused}/bin/sed 's/\xC2\xA0/ /g' -i $(find . -name '*.md')
      '';
      default = caaso-site;
    };

    apps = rec {
      caaso-site-serve = {
        type = "app";
        program = "${packages.caaso-site-serve}/bin/serve";
      };
      default = caaso-site-serve;
    };
  }) // {
      };
}
