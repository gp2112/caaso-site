---
layout: page
title: Diretoria
permalink: /diretoria
---

O CAASO representa todos os estudantes da USP de São Carlos e é gerido por uma Diretoria, eleita anualmente pelo corpo discente da universidade; assim, é fundamental que os diretores estejam sempre acessíveis aos alunos.


#### Gestão Atual: CAASO Popular

<center>
<img alt="Gestao CAASO Popular" src="/assets/imgs/caaso-popular.jpg" width="800"/>
</center>
Formada pela União da Juventude Comunista (UJC) e pelo Movimento por uma Universidade Popular (MUP), seguimos uma linha politicamente combativa, de esquerda radical, buscando resgatar o grande histórico de lutas que o CAASO possui. Nosso programa pode ser conferido [aqui](https://drive.google.com/file/d/1Tcv5fZqG0CneHR9zBDEvt_p5cAeukauA/view).
