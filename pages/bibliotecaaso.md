---
layout: page
title: BiblioteCAASO
permalink: /bibliotecaaso
---

A **BiblioteCAASO** é um espaço público de aprendizado e boa convivência. Visitantes podem sentar no sofá e desfrutar de periódicos, gibis, livros de variados temas ou simplesmente sentar e conversar ou ouvir as histórias da bibliotecária, **Tia Celly**, que organiza e administra esta biblioteca desde 1980. Logo na entrada da Biblioteca há o guarda-volumes, onde os usuários devem deixar suas mochilas, canecas, guarda chuvas, entre outros, a fim de proteger e conservar o acervo da Biblioteca.

Os **sócios CAASO** poderão desfrutar de tudo isso, além de levar os livros e filmes para casa! O usuário pode retirar até dois livros durante um mês; até três nas férias de julho; e até seis livros no final do ano cujo prazo de devolução é abril do ano seguinte.

---

### Acervo

Uma parte do acervo inclui edições da **Folha de São Paulo**, **Veja**, **Mundo Estranho**, **Superinteressante**, **Problemas Brasileiros**; os **gibis Calvin**, **Akira**, **Sandman**, **Asterix**, **Angeli**, **Glauco**, **Laerte**, **mangás**, **HolyAvenger**, **Elektra**, **V de Vingança**, **Mafalda**, e, principalmente, os livros de temas variados: **literatura brasileira**, **literatura estrangeira**, **cinema**, **filosofia**, **sociologia**, **educação**, **esporte**, **música – conteúdo com partituras de violão e piano** -, **coleções (Primeiros Passos, Tudo É História, Os Pensadores, Os Economistas)**, **Pasquim, dicionários**, entre outros.

Para mais informações acesse a página do [Facebook](http://www.facebook.com/bibliotecaaso).

---

### Cadastro

Para cadastro na BiblioteCAASO, são necessárias **1 foto 3×4** e **Carteirinha de Sócio CAASO dentro da validade**.

---

### Horário de Funcionamento

- **Período letivo:** de segunda à sexta – das 11h00 às 14h00; das 14h30 às 17h15 e das 18h às 19h15.
- **Durante as férias:** de segunda à sexta – das 09h30 às 14h00 e das 14h30 às 18h00.
