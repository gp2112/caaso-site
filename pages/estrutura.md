---
layout: page
title: Estrutura
permalink: /estrutura
---

## BiblioteCAASO

A **BiblioteCAASO** é um espaço público de aprendizado e boa convivência. Visitantes podem sentar no sofá e desfrutar de periódicos, gibis, livros de variados temas ou simplesmente sentar e conversar ou ouvir as histórias da bibliotecária, **Tia Celly**, que organiza e administra esta biblioteca desde 1980. Logo na entrada da Biblioteca há o guarda-volumes, onde os usuários devem deixar suas mochilas, canecas, guarda chuvas, entre outros, a fim de proteger e conservar o acervo da Biblioteca.

Os **sócios CAASO** poderão desfrutar de tudo isso, além de levar os livros e filmes para casa! O usuário pode retirar até dois livros durante um mês; até três nas férias de julho; e até seis livros no final do ano cujo prazo de devolução é abril do ano seguinte.

### Acervo

Uma parte do acervo inclui edições da **Folha de São Paulo**, **Veja**, **Mundo Estranho**, **Superinteressante**, **Problemas Brasileiros**; os **gibis Calvin**, **Akira**, **Sandman**, **Asterix**, **Angeli**, **Glauco**, **Laerte**, **mangás**, **HolyAvenger**, **Elektra**, **V de Vingança**, **Mafalda**, e, principalmente, os livros de temas variados: **literatura brasileira**, **literatura estrangeira**, **cinema**, **filosofia**, **sociologia**, **educação**, **esporte**, **música – conteúdo com partituras de violão e piano** -, **coleções (Primeiros Passos, Tudo É História, Os Pensadores, Os Economistas)**, **Pasquim, dicionários**, entre outros.

Para mais informações acesse a página do [Facebook](http://www.facebook.com/bibliotecaaso).

### Cadastro

Para cadastro na BiblioteCAASO, são necessárias **1 foto 3×4** e **Carteirinha de Sócio CAASO dentro da validade**.

### Horário de Funcionamento

- **Período letivo:** de segunda à sexta – das 11h00 às 14h00; das 14h30 às 17h15 e das 18h às 19h15.
- **Durante as férias:** de segunda à sexta – das 09h30 às 14h00 e das 14h30 às 18h00.

---

## Salão de Jogos

O Centro Acadêmico ideal é aquele que faz com que você se sinta em casa. É por esse e outros motivos que o CAASO conta com um Salão de Jogos **completo** e **acessível** para os seus **sócios**. Nossa infra-estrutura conta com **mesa de ping pong**, **pebolim**, **jogos de cartas**, **videogame** e – é claro que não poderia faltar – duas mesas de sinuca, palcos de partidas memoráveis há gerações. Se você não gosta de nada disso, não tem problema, é só se acomodar em um dos sofás disponíveis e ligar a televisão enquanto aguarda seus amigos acabarem de jogar ou enquanto espera o horário da próxima aula.
Assim como diversos outros elementos da nossa estrutura, o Salão de Jogos carece de manutenção regular para garantir que continue sendo uma opção de lazer agradável. Por isso, precisamos da ajuda de vocês, estudantes do CAASO! Passem na diretoria e façam a sua carteirinha de Sócio CAASO, dessa forma poderemos manter e melhorar nosso Salão de Jogos para que ele permaneça sendo um local de descontração e de qualidade!
Já é Sócio e ficou com vontade de jogar? É só aparecer na diretoria com a sua carteirinha em mãos e pedir o material do que você quiser jogar! Se já tiver gente jogando, melhor ainda! Aproveite para se enturmar e fazer novas amizades, afinal uma das principais funções de um Centro Acadêmico também é a de integrar os seus membros!
O Salão de Jogos fica aberto de **segunda à sexta**, no mesmo horário da diretoria. Esperamos vocês lá!

---

## Salão Cultural

Em São Carlos, a vida cultural é bem ativa com diversas atividades propostas tanto pelas universidades (USP e UFSCar) quanto pela Prefeitura Municipal, SESC, entre outros. Deve-se compreender que os espaços de formação vão muito além do mundo acadêmico – das salas e laboratórios – e que essa formação unilateral oferecida nos cursos não basta. É fundamental não reprimir a curiosidade intuitiva de entender o mundo a sua volta, conhecer diferentes culturas, ter a percepção sobre os diferentes espaços a sua volta.

A organização coletiva dos estudantes para realização de determinada atividade torna-se um instrumento transformador. O CAASO apóia essas iniciativas, oferecendo espaços e infra-estrutura sempre que possível e buscando enriquecer a diversidade artístico-cultural no campus e na cidade. Por exemplo, o Salão Cultural, em frente à BiblioteCAASO, proporciona aulas de Dança de Salão, Forró, encontros de Capoeira, Yoga, Kendo, Teatro e Taiko. Aproveite esse espaço para trocar experiências, PARTICIPAR, pensar, TRANSFORMAR, criar.

Se há algo que você gosta e não é contemplado por essas atividades, por que não começar uma prática nova?

### Quer usar os espaços do CAASO?

Para solicitar o uso de espaços gerenciados pelo nosso Centro Acadêmico é só preencher o forms aqui. Faça isso com alguma antecedência pra gente conseguir se organizar 😉
