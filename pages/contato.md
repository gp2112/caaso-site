---
title: Contato
layout: page
permalink: /contato
---

Por representar todos os alunos do Campus de São Carlos da USP, o Centro Acadêmico Armando de Salles Oliveira deve estar sempre em constante contato com os estudantes. Fale com o CAASO através das redes sociais, por e-mail, ou fisicamente nos espaços de debate e conversa do campus.

### Secretaria do CAASO

**Horário de funcionamento:** segunda a sexta-feira, das 10h às 17h.

**Telefones:** (16) 3371-9699 / (16) 3373-9654

**E-mail:** [caaso@caaso.org.br](mailto:caaso@caaso.org.br)

<center>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.557415255522!2d-47.89721658364115!3d-22.006910699835256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b870cb59f6dda9%3A0xb7ea09e89dcb44de!2sCAASO%20Centro%20Acad%C3%AAmico%20Armando%20de%20Salles%20Oliveira!5e0!3m2!1spt-BR!2sbr!4v1684870297567!5m2!1spt-BR!2sbr" width="800" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</center>
