---
layout: page
title: Entidades
permalink: /entidades
---

## Secretarias Acadêmicas

Na USP de São Carlos, há algumas peculiaridades com relação às entidades de representação dos estudantes: temos um Centro Acadêmico que representa os alunos de todo o campus, o CAASO; e temos Secretarias Acadêmicas, SAs, que são a representação específica dos estudantes de um curso ou de um conjunto de cursos de uma mesma unidade.

O CAASO surgiu como o CA dos alunos da EESC (Escola de Engenharia de São Carlos). A primeira unidade do campus. Com o tempo a Escola de Engenharia sofreu desmembramentos que deram origem às demais unidades: IFSC, IQSC, ICMC e, no ano passado, IAU (Institutos de Física, Química, Ciências Matemática e Computação, e Arquitetura e Urbanismo, respectivamente). Contudo, os estudantes não criaram outros CAs, mantendo o CAASO como entidade representativa.

Com um aumento expressivo do número de alunos e cursos neste campus, o CAASO passou a ter dificuldades em conhecer e atender as demandas específicas destes e em levar as discussões, lutas e atividades a todos. Nesse contexto, e também por necessidade de um novo campo de ação política, é que surgem e têm se consolidado uma nova forma de representação: as SAs. Na prática, elas são como os CAs que podemos encontrar nos outros campi da USP, mas para não rolar uma confusão com o nome e o papel do CAASO, apareceu essa denominação de Secretaria Acadêmica.

Portanto, reconhecemos as SAs como entidades estudantis legítimas, autônomas e de papel complementar ao CAASO. Mas é fundamental que elas se reconheçam enquanto tal e que os estudantes que representa se reconheçam nela. Para isso, além de desenvolver atividades de caráter sociocultural que promovam a integração, é muito importante participar dos fóruns e espaços de formação do Movimento Estudantil  – como por exemplo assembleias, congressos, CCAs e CSAs – e promover palestras, discussões, debates e lutas sobre questões específicas e gerais do curso, da universidade e da sociedade.


{% for entidade in site.data.entidades %}
- [{{entidade.name}}]({{entidade.url}})
{% endfor %}

## DCE Livre da USP

O **Diretório Central dos Estudantes Livre da USP “Alexandre Vannucchi Leme” (DCE)** é a entidade de representação dos estudantes de graduação de todos os campi da USP (Bauru, Butantã, USP Leste, Piracicaba, Pirassununga, Lorena, Ribeirão Preto e São Carlos), com sede localizada em São Paulo. Isso significa que o DCE é responsável por organizar os espaços do Movimento Estudantil e validar as decisões tomadas pelo conjunto estudantil da USP, sejam elas referentes à universidade, ou mais amplamente, à sociedade.

Fechado pela ditadura militar, o DCE da USP foi refundado em 1976, momento em que se acrescentou ao seu nome o termo “Livre” e, também, a homenagem a Alexandre Vannucchi Leme, estudante de Geologia da USP assassinado sob tortura pelos militares em 1973.

Todos os estudantes da USP têm direito ao voto direto e à candidatura em uma chapa para a eleição anual. As gestões organizam-se de forma a construir as seguintes decisões dos fóruns do movimento estudantil:

- **Congressos dos Estudantes:** a cada dois anos, são discutidas e definidas as diretrizes de todo o movimento estudantil da USP.

- **Assembléias Gerais dos estudantes da USP:** espaços de discussão do posicionamento e da ação dos estudantes acerca de assuntos específicos, nos quais todo estudante tem direito a voz e voto.

- **Conselho de Centros Acadêmicos (CCA):** reunião periódica entre os representantes do DCE e dos CAs, em que participam o CAASO e as Secretarias Acadêmicas do campus para discutir e organizar políticas, ações e reivindicações dos estudantes.

> “[Alexandre Vanucchi Leme](https://pt.wikipedia.org/wiki/Alexandre_Vannucchi_Leme) morreu aos 22 anos de idade. Cursava o quarto ano de Geologia na USP, e seu apelido era Minhoca. Pertencia ao Centro Acadêmico da Geologia (CEPEGE- Centro de Pesquisas Geológicas), e ajudou na reconstrução do DCE na clandestinidade. Foi preso, torturado até a morte e enterrado como indigente, com falso laudo médico assinado pelo médico Isaac Abramovitc, laudo contestado em 1991 por CPI. A família de Vanucchi Leme foi indenizada, e o pai conseguiu finalmente reaver os ossos do filho em 1983, quando então sua família o enterrou de acordo com sua cultura católica em Sorocaba onde nasceu.” – Trecho do livro “CALE-SE”, de Caio Túlio Costa (editora A Girafa. Seg. Edição, 2003)


## UNE - União Nacional dos Estudantes

Nacionalmente, temos como entidade representativa a conhecida UNE (União Nacional dos Estudantes), que teve importante participação em diversas lutas, tanto na defesa da universidade pública quanto nos grandes temas em pauta no país em cada época.

A UNE tem uma diretoria que é eleita nos CONUNEs (Congressos da entidade, nos quais os estudantes participam através da eleição de delegados), que ocorrem a cada dois anos. Há também os CONEBs (Congressos de entidades de base), que reúnem os Cas.

Hoje, infelizmente, a UNE tem tido uma atuação bastante limitada e espaços de discussão e decisão pouco comprometidos, que reproduzem muitos vícios de uma cultura política que não preza pelo debate e pela construção coletiva, que aceita uma lógica de “acordos de cúpula” e que se atrela, muitas vezes, a grupos, partidos ou governos que acabam “aparelhando” o movimento, ou seja, utilizando a entidade para seus interesses.

Costumamos dizer, nesse sentido, que a UNE se afastou dos estudantes. Mas isso só aconteceu porque, de várias maneiras, os estudantes também se afastaram da UNE.  Formou-se um círculo vicioso: cada vez menos estudantes se interessaram a participar de seus espaços; isso permitiu que a diretoria da entidade se afastasse do cotidiano do movimento estudantil; e esse distanciamento, portanto, aumentou o desinteresse dos estudantes pela participação na entidade.

Só a partir da própria dinâmica do movimento estudantil podemos superar essa situação: ampliar nossa organização e nossa atuação, nas SAs, CAASO, DCE e todos os espaços do ME, fortalecer os laços e as lutas do movimento estudantil nacional e, a partir daí, revalorizar e reocupar os espaços da UNE.
