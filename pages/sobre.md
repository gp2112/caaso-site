---
layout: page
title: Sobre
permalink: /sobre
tags: [Page]
---


Um dos maiores centros acadêmicos da América Latina, o Centro Acadêmico Armando de Salles Oliveira, mais conhecido como CAASO, é o órgão representativo de todos os alunos de graduação da USP São Carlos. O CAASO foi fundado no dia 22 de abril de 1953 pela primeira turma de alunos da Escola de Engenharia de São Carlos e recebeu este nome em homenagem ao governador do Estado de São Paulo que criou a Universidade de São Paulo.

Ao longo dos anos, o CAASO teve uma grande importância no movimento de redemocratização do Brasil, manifestando-se nos momentos mais cruciais de nossa história, como após o suicídio de Getúlio Vargas em 1954, a renúncia do presidente Jânio Quadros em 1961, o estado de sítio decretado pelo presidente João Goulart em 1963, o golpe militar depondo este mesmo presidente em 1964, entre outros episódios, exigindo o respeito às leis e às liberdades democráticas. Em diversos momentos, os alunos decretaram greve reivindicando melhores condições de ensino, participação discente nos órgãos colegiados, fim da violência contra os estudantes, entre outros motivos.

O CAASO possui diversos grupos culturais, dos quais qualquer aluno pode participar e se expressar, como o Grupo de Som, o FOCA, a GAPeria, a Atlética, o Cineclube, o Coletivo de Mulheres, o Teatro ACASO, e ainda alguns grupos desativados por falta de pessoal, mas que podem logo voltar à ativa, como o Grupo de Silk, Grupo do Jornal e a Rádio. Para se informar sobre os grupos culturais, procure a Secretaria do CAASO ou os seus integrantes. Possuímos também uma biblioteca, a BiblioteCAASO, criada em 1954 e que possui um acervo com mais de 20.000 livros, além de revistas, jornais, gibis, CDs e vídeos. Há ainda o Salão Cultural, que abriga as salas de vários grupos culturais como a Capoeira, o Taiko, o Forró, a Dança de Salão, o Yoga e o Kendo, e também o Salão de Jogos, que possui diversos tipos de jogos como ping-pong, pebolim, futebol de botão, sinuca, fliperama e ainda serve de espaço para discussões entre os estudantes.

O CAASO é administrado por uma diretoria eleita anualmente, a atual diretoria 2022-2023 é a Gestão CAASO Popular. Porém quem manda no CAASO é você. O órgão de decisão máximo do Centro Acadêmico é a Assembléia Geral, na qual através do voto, todos nós decidimos como tudo deve ser. Para ter acesso a tudo que se passa no CAASO e notícias das diversas atividades desenvolvidas pelos alunos fique ligado nesse site.

Para usufruir toda a estrutura que o CAASO lhe oferece, e ainda ajudar a mantê-la e ampliá-la, faça a carteirinha de sócio, a qual ainda lhe dará acesso a diversas festas na faixa que são organizadas ao longo do ano pela Diretoria e pelos grupos culturais.

## Minerva - Nosso Símbolo

A deusa grega Minerva (também conhecida como Palas Atena) é o símbolo do nosso Centro Acadêmico. É ela que aparece, ainda que estilizada, no logotipo do CAASO. A Minerva é o símbolo oficial dos engenheiros, razão esta pela qual se tornou o símbolo do CAASO, já que os primeiros cursos da USP em São Carlos eram de engenharia.

Minerva é deusa guerreira, mas, ao mesmo tempo, deusa da sabedoria e da reflexão. Ela não vence seus inimigos pela força bruta, mas pelos ardis que inventa, pela astúcia e pela inteligência de seus estratagemas. É a senhora das técnicas, da racionalidade instrumental e a criadora das saídas de engenhosidade.

Deusa guerreira, da sabedoria, das atividades práticas, mas também do trabalho artesanal de fiação, do espirito criativo e da vida especulativa, ela reúne aspectos fundamentais à formação do engenheiro. A Minerva sintetiza duas dimensões do trabalho do engenheiro: a criação e a execução.

>“Minerva era filha de Júpiter, que após engolir a deusa Métis (Prudência), com uma forte dor de cabeça, pediu a Vulcano que abrisse sua cabeça com o seu melhor machado, de onde saiu Minerva. Já adulta, portando escudo, lança e armadura, passa a ser considerada uma das três deusas virgens, ao lado de Diana e Vesta.
>Por ter saído da cabeça de Júpiter, Minerva passa a ser conhecida como a deusa da sabedoria, das artes e da estratégia de guerra. Minerva e Netuno disputaram entre si qual dos dois daria o nome à cidade que Cécropes, rei dos atenienses, havia mandado construir na Ática. E essa honra caberia àquele que realizasse a maior proeza, em beleza e significado. Minerva, com um golpe de lança, fez nascer da terra uma oliveira em flor, e Netuno, com um golpe do seu tridente, fez nascer um cavalo alado e fogoso. Os deuses, que presidiram a este duelo, decidiram em favor de Minerva, já que a oliveira florida, além de muito bela, era o símbolo da paz. Assim, a cidade nova da Ática foi chamada Atenas. A Minerva era para os ateanienses a deusa da excelência, da misericórdia e da pátria”
